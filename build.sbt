name := "scruid"

scalaVersion := "2.11.2"

val circeVersion = "0.9.3"

version := "1.0.5-SNAPSHOT"

libraryDependencies ++= Seq(
  "com.typesafe"      % "config"           % "1.3.3",
  "io.circe"          %% "circe-core"      % circeVersion,
  "io.circe"          %% "circe-parser"    % circeVersion,
  "io.circe"          %% "circe-generic"   % circeVersion,
  "io.circe"          %% "circe-java8"     % circeVersion,
  "com.typesafe.akka" %% "akka-http"       % "10.1.1",
  "de.heikoseeberger" %% "akka-http-circe" % "1.20.1",
  "ca.mrvisser"       %% "sealerate"       % "0.0.5",
  "ch.qos.logback"    % "logback-classic"  % "1.1.11",
  "org.scalactic"     %% "scalactic"       % "3.0.5",
  "org.scalatest"     %% "scalatest"       % "3.0.5" % "test"
)

resolvers += Resolver.sonatypeRepo("releases")